export const ADD_TO_CART = "ADD_TO_CART";

export const DELETE_SHOE = "DELETE_SHOE";

export const CHANGE_QUANTITY = "CHANGE_QUANTITY";