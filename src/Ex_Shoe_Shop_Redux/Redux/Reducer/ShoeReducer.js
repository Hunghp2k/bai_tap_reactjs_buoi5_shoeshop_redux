import { data_shoe } from "../../data_shoe";
import { ADD_TO_CART, DELETE_SHOE, CHANGE_QUANTITY } from "../constant/ShoeConstant";

let initiaValue = {
    listShoe: data_shoe,
    cart: [],
}
export const shoeReducer = (state = initiaValue, action) => {
    switch (action.type) {
        case ADD_TO_CART: {
            let cloneCart = [...state.cart];
            let index = cloneCart.findIndex((item) => {
                return item.id == action.payload.id;
            });
            if (index == -1) {
                let newshoe = { ...action.payload, soLuong: 1 };
                cloneCart.push(newshoe);
            } else {
                cloneCart[index].soLuong++;
            }
            return { ...state, cart: cloneCart };
        }
        case DELETE_SHOE: {
            let newCart = state.cart.filter((item) => {
                return item.id != action.payload
            })
            return { ...state, cart: newCart };
        }
        case CHANGE_QUANTITY: {
            let cloneCart = [...state.cart]
            let index = cloneCart.findIndex((item) => {
                return item.id == action.payload.idShoe;
            });
            cloneCart[index].soLuong += action.payload.luachon;
            return { ...state, cart: cloneCart }
        }
        default:
            return state;
    }
};
