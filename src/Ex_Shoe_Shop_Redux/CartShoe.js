import React, { Component } from 'react'
import { connect } from 'react-redux'

class CartShoe extends Component {
    renderTbody = () => {
        return this.props.cart.map((item, index) => {
            return (

                <tr key={index} style={{ lineHeight: "3.5" }}>
                    <td>{item.id}</td>
                    <td>{item.name}</td>
                    <td>{item.price * item.soLuong}</td>
                    <td>
                        <button onClick={() => { this.props.handleChangeQuantityShoe(item.id, -1) }} className='btn btn-danger'>-</button>
                        <strong>{item.soLuong}</strong>
                        <button onClick={() => { this.props.handleChangeQuantityShoe(item.id, 1) }} className='btn btn-success'>+</button>
                    </td>

                    <td>
                        <img style={{ width: 50 }} src={item.image} alt="" />
                    </td>
                    <td>
                        <button onClick={() => { this.props.handleDeleteShoe(item.id) }} className='btn btn-danger'>Delete</button>
                    </td>
                </tr>
            )
        })
    }
    render() {
        return (
            <div>
                <table className='table'>
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Price</th>
                            <th>Quantity</th>
                            <th>Img</th>
                        </tr>
                    </thead>
                    <tbody>{this.renderTbody()}</tbody>
                </table>
            </div>
        )
    }
}
let mapStateToProps = (state) => {
    return {
        cart: state.shoeReducer.cart,
    };
};
let mapDispatchToProps = (dispatch) => {
    return {
        handleDeleteShoe: (idShoe) => {
            let action = {
                type: "DELETE_SHOE",
                payload: idShoe,
            };
            dispatch(action);
        },
        handleChangeQuantityShoe: (idShoe, luachon) => {
            let action = {
                type: "CHANGE_QUANTITY",
                payload: { idShoe: idShoe, luachon }
            }
            dispatch(action);
        }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(CartShoe)