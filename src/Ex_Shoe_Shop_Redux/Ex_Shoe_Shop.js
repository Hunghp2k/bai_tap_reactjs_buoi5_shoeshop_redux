import React, { Component } from 'react'
import CartShoe from './CartShoe'
import { data_shoe } from './data_shoe'
import ListShoe from './ListShoe'

export default class Ex_Shoe_Shop extends Component {
    render() {
        return (
            <div className='container'>
                <h2 className='text-center'>Ex_Shoe_Shop</h2>
                <CartShoe />
                <ListShoe />
            </div>
        )
    }
}
