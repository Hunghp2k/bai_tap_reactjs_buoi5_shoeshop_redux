import React, { Component } from 'react'
import { connect } from 'react-redux';

class ItemShoe extends Component {
    render() {
        let { image, name, price } = this.props.shoe;
        return (
            <div className="card col-3">
                <img src={image} className="card-img-top" alt="..." />
                <div className="card-body">
                    <h5 className="card-title">{name}</h5>
                    <p className="card-text">{price}</p>
                    <a onClick={() => { this.props.handlePushToCart(this.props.shoe) }} href="#" className="btn btn-primary">Add</a>
                </div>
            </div>
        );
    }
}
let mapDispatchToProps = (dispatch) => {
    return {
        handlePushToCart: (shoe) => {
            let action = {
                type: "ADD_TO_CART",
                payload: shoe,
            };
            dispatch(action);
        },
    };
};
export default connect(null, mapDispatchToProps)(ItemShoe)