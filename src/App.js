import logo from './logo.svg';
import './App.css';
import Ex_Shoe_Shop from './Ex_Shoe_Shop_Redux/Ex_Shoe_Shop';

function App() {
  return (
    <div >
      <Ex_Shoe_Shop />
    </div>
  );
}

export default App;
